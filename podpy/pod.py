'''
Proper Orthogonal Decomposition main class
'''

import numpy as np
import scipy as sp

class POD:
    ''' POD class '''
    
    def __init__(self, 
        snapshots,
        inner_product = None,
        epsilon       = 0, 
        N_modes       = 5,
        error_type    = "absolute"
    ):
        ''' Default setter '''
        # Data set
        self.snapshots = snapshots
        self.d         = np.shape(snapshots)[0]
        self.K         = np.shape(snapshots)[1]

        # Threshold vs number of modes
        if epsilon <= 0:
            self.epsilon = 0
            self.N       = N_modes
        else:
            self.epsilon = epsilon
            self.N       = 0

        # Inner product matrix
        if inner_product is not None:
            self.X = inner_product
        else:
            self.X = np.eye(np.shape(snapshots)[0])

        # Options
        self.error_type = error_type

    def __str__(self):
        ''' POD class print '''
        # Basic metadata
        print_info  = "Dimension          : {}\n".format(self.d)
        print_info += "Number of snapshots: {}\n".format(self.K)
        print_info += "Compression tol.   : {}\n".format(self.epsilon)
        print_info += "Number of modes    : {}\n".format(self.N)
        print_info += "Error type         : {}\n".format(self.error_type)

        # Check reduction status
        if np.shape(self.sigma)==0:
            print_info += "Reduced            : False [use .reduced() method]"
        else:
            print_info += "Reduced            : True"
        return print_info

    def reduce(self):
        # --- Correlation matrix
        self.C = self.snapshots.T @ self.X @ self.snapshots

        # --- Relative error case
        if self.error_type == 'absolute':
            epsilon = self.epsilon
        else:
            epsilon = self.epsilon * np.min(np.diag(self.C))

        # --- Threshold based
        if self.N == 0: 
            # LB relevant spectrum
            l_min = np.max([ epsilon / self.C.shape[0], 1e-16])
            # Compute relevant eig values
            S     = sp.linalg.eigvalsh(self.C, 
                                       subset_by_value = (l_min, np.inf) 
                                       )
            # Reach the threshold
            self.N = np.sum( np.cumsum( S ) >= epsilon )

        else: # Size based /!/ epsilon not computed /!/
            self.epsilon = None
        
        # --- Compute the eigen values/vectors needed
        Idx  = [self.K-self.N, self.K-1]
        S, U = sp.linalg.eigh(self.C, subset_by_index = Idx )
        
        # --- Setting the reduced basis
        self.phi   = U / np.sqrt(S)
        self.rb    = self.snapshots @ self.phi
        self.sigma = np.flip(S)


'''
Projection error function
'''

def projection_error(Uh, Zh, Xh, error_type = "absolute"):
    '''
    projection_error(Uh, Zh, Xh, error_type="absolute")
        Compute different projection errors of Uh over Zh with respect to
        the Xh inner product.

        Parameters
        ----------
        Uh : (Ndof x Nsnap) array, data to project.
        Zh : (Ndof x Nz) array, basis of the set to project onto.
        Xh : (Ndof x Ndof) sparse matrix/array, inner product.
        error_type : "absolute" or "relative".

        Returns
        -------
        e_total : float
            Sum of the squares of the projection differences in the Xh norm,
            this error correspond directly to the POD optimality criterion.
        e_mean : float
            Avarage equivalent of the total error (e_total), gives a more 
            interpretable version of mean error.
        e_list : (1 x Nsnap) array
            Array containing all the individual projection errors w.r.t.
            the Xh norm.
        
    '''
    # --- Init.
    e_list = []
    DPUh  = Uh - Zh@((Zh.T@Xh)@Uh)
    XDPUh = Xh @ DPUh

    # --- Snapshots loop
    for i in range(np.shape(Uh)[1]):
        e_list.append( DPUh[:,i] @ XDPUh[:,i] )
    e_list = np.array( e_list )

    # --- Relative error
    if error_type != "absolute":
        XDPUh = Xh @ Uh
        for i in range(np.shape(Uh)[1]):
            e_list[i] /= Uh[:,i] @ XDPUh[:,i]
    
    # --- Different projection errors
    e_total = np.sum( e_list)
    e_mean  = np.sqrt( np.mean( e_list ) )
    e_list  = np.sqrt( e_list )

    return [e_total, e_mean, e_list]

###  % --- Adapt epsilon in relative case
###  if strcmpi('relative',pod_norm)
###    Unorm = zeros(size(Uh,2),1);
###    for i = 1:size(Uh,2)
###      Unorm(i) = sqrt(Uh(:,i)'*Xh*Uh(:,i));
###    end
###    epsilon = epsilon * min(Unorm);
###  end