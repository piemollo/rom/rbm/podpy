"""
POD for Python
"""

__all__ = [
    "pod",
]

from .pod import POD
