# Proper Orthogonal Decomposition for Python

## To build the project

First download the module repository using the command
````bash
git clone https://gitlab.com/piemollo/rom/rbm/podpy.git
````
then add the module in the local Python Packager (PIP)
using the following command
```bash
> pip install -e .
```
Note the option `-e` stands for editable and allow local modifications.

The module is now callable from any other scripts as for usual modules 
```python
import pytemplate as pyt
```
or in any other usual manner.

## Description

This module profides a class object to perform Proper Orthogonal 
Decompsosition (POD) on dataset and also profides handy way to manage options, save and load results.
