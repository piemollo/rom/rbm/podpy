import numpy as np
from podpy import POD

d = 20 # dimension
K = 10 # number of snapshots

# Data set
np.random.seed(12)
A = np.random.rand(d,K)

# Test optional arguments
pod_test = POD(A)
pod_test = POD(A, inner_product=0)       # Prescribe inner product matrix
pod_test = POD(A, epsilon=0)             # Compression tolerance
pod_test = POD(A, N_modes=5)             # Nb retained modes
pod_test = POD(A, error_type="relative") # Type reduction: absolut/relative
pod_test = POD(A, large_ds=False)        # Large data set: True/False
pod_test = []

# Unitary test: All cases here should give the same result
pod_test.append( POD(A, large_ds=True , epsilon=1e-3) )
pod_test.append( POD(A, large_ds=True , N_modes=5)    )
pod_test.append( POD(A, large_ds=False, epsilon=1e-3) )
pod_test.append( POD(A, large_ds=False, N_modes=5)    )

for instance in pod_test:
    instance.reduce()
    print(instance.rb.T@instance.rb)
